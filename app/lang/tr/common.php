<?php

return array(

	'yes'				=> 'Evet',
	'no'				=> 'Hayır',
	'login'				=> 'Giriş Yap',
	'register'			=> 'Kayıt Ol',
	'username'			=> 'Kullanıcı Adı',
	'password'			=> 'Şifre',
	'rememberme'		=> 'Beni hatırla',
	'enterpassword'		=> 'Şifrenizi Giriniz',
	'enterusername'		=> 'Kullanıcı Adınızı Giriniz'

);