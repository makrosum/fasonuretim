<aside id="sidebar_left" class="nano nano-primary">
    <div class="nano-content">

        <!-- Sidebar Header -->
        <header class="sidebar-header">

            <!-- User-menu Dropdown -->
            <div class="user-menu">
                <div class="row text-center mbn">
                    <div class="col-xs-4">
                        <a href="../dashboard.html" class="text-primary" data-toggle="tooltip" data-placement="top" title="Dashboard">
                            <span class="glyphicons glyphicons-home"></span>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="../pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="Messages">
                            <span class="glyphicons glyphicons-inbox"></span>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="../pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="Tasks">
                            <span class="glyphicons glyphicons-bell"></span>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="../pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="Activity">
                            <span class="glyphicons glyphicons-imac"></span>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="../pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicons glyphicons-settings"></span>
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="../pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="Cron Jobs">
                            <span class="glyphicons glyphicons-restart"></span>
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <!-- End: User-menu Dropdown -->

        <!-- Sidebar Menu -->
        <ul class="nav sidebar-menu">

            <!-- Single Link -->
            <li class="sidebar-label pt20">Link</li>
            <li>
                <a href="#">
                    <span class="fa fa-calendar"></span>
                    <span class="sidebar-title">Default Link</span>
                </a>
            </li>

            <!-- Link with Label -->
            <li class="sidebar-label pt20">Link with Label</li>
            <li>
                <a href="#">
                    <span class="fa fa-calendar"></span>
                    <span class="sidebar-title">With Label</span>
                    <span class="sidebar-title-tray">
                        <span class="label label-xs bg-primary">New</span>
                    </span>
                </a>
            </li>

            <!-- Link With Active Class -->
            <li class="sidebar-label pt20">Link with Active</li>
            <li class="active">
                <a href="#">
                    <span class="glyphicons glyphicons-book_open"></span>
                    <span class="sidebar-title">Active Link</span>
                </a>
            </li>

            <!-- Link with Dropdown -->
            <li class="sidebar-label pt15">With Dropdown</li>
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="glyphicons glyphicons-fire"></span>
                    <span class="sidebar-title">First Level</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="#">
                            <span class="glyphicons glyphicons-check"></span> SubMenu Item 1 </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="glyphicons glyphicons-check"></span> SubMenu Item 2 </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="glyphicons glyphicons-check"></span> SubMenu Item 3 </a>
                    </li>
                </ul>
            </li>

            <!-- Link with Multi-level Dropdown -->
            <li class="sidebar-label pt15">With Multi-Level</li>
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="glyphicons glyphicons-more_items"></span>
                    <span class="sidebar-title">First Level</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="glyphicons glyphicons-cogwheels"></span> Second Level
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="#"> Third Level Item 1 </a>
                            </li>
                            <li>
                                <a href="#"> Third Level Item 2 </a>
                            </li>
                            <li>
                                <a href="#"> Third Level Item 3 </a>
                            </li>
                        </ul>
                    </li>
              
                </ul>
            </li>

            <!-- Sidebar Notes/Bullets -->
            <li class="sidebar-label pt20">Sidebar Notes</li>
            <li class="sidebar-proj">
                <a href="#projectOne">
                    <span class="fa fa-dot-circle-o text-primary"></span>
                    <span class="sidebar-title">Sidebar Note 1</span>
                </a>
            </li>
            <li class="sidebar-proj">
                <a href="#projectTwo">
                    <span class="fa fa-dot-circle-o text-info"></span>
                    <span class="sidebar-title">Sidebar Note 2</span>
                </a>
            </li>

            <!-- Sidebar Progress Bars -->
            <li class="sidebar-label pt25 pb10">Sidebar Stats</li>
            <li class="sidebar-stat mb10">
                <a href="#projectOne" class="fs11">
                    <span class="fa fa-inbox text-info"></span>
                    <span class="sidebar-title text-muted">Sidebar Stat</span>
                    <span class="pull-right mr20 text-muted">35%</span>
                    <div class="progress progress-bar-xs ml20 mr20">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 35%">
                            <span class="sr-only">35% Complete</span>
                        </div>
                    </div>
                </a>
            </li>
        </ul>

        <!-- Sidebar Toggle Collapsed State Btn -->
        <div class="sidebar-toggle-mini">
            <a href="#">
                <span class="fa fa-sign-out"></span>
            </a>
        </div>
    </div>
</aside>