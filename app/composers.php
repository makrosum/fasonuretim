<?php

/*
| View Composers
*/

View::composer('partials.sidebar', 'Extra\Composers\SidebarComposer');
View::composer('partials.header', 'Extra\Composers\HeaderComposer');
View::composer('partials.topbar', 'Extra\Composers\TopbarComposer');