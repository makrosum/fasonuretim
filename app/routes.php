<?php

/*
| Routes
*/



/*
| Drafts (Temprorary Views)
*/

Route::group(array('prefix' => 'draft'),function()
{
	Route::get('home',function()
	{ 
		return View::make('drafts.home')->withTitle('Home'); 
	});
	Route::get('login',function()
	{ 
		return View::make('drafts.login')->withTitle('Login'); 
	});
});

/*
| Filters,Patterns etc.
*/

Route::pattern('id','[0-9]+');
Route::when('*', 'csrf', array('post', 'put', 'delete'));